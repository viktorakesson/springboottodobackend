package com.example.nativecrud;

import com.example.nativecrud.entities.Todo;
import com.example.nativecrud.repositories.TodoRepositories;
import com.example.nativecrud.services.TodoService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class NativeCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(NativeCrudApplication.class, args);
    }

    @Bean
    CommandLineRunner init(TodoRepositories todoRepositories) {
        return args -> {
        //    todoRepositories.saveAll(List.of(new Todo("Todo 1 exempel"), new Todo("Todo 2 exempel")));
        //    todoRepositories.findAll().forEach(System.out::println);
        };
    }
}
