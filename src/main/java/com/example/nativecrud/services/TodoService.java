package com.example.nativecrud.services;

import com.example.nativecrud.dto.AddTodoRequestDto;
import com.example.nativecrud.entities.Todo;
import com.example.nativecrud.repositories.TodoRepositories;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    private final TodoRepositories todoRepositories;

    public TodoService (TodoRepositories todoRepositories) {
        this.todoRepositories = todoRepositories;
    }

    public List<Todo> findAll(){
        return todoRepositories.findAll();
    }

    public Todo addNewTodo(AddTodoRequestDto addTodoRequestDto) {
        Todo todo = new Todo(addTodoRequestDto.title());
        return todoRepositories.save(todo);
    }

    public void deleteById(int id) {
        todoRepositories.deleteById(id);
    }

    public Todo getTodoById(int id) {
        return todoRepositories.findById(id).orElseThrow();
    }

    public void updateById(int id) {

        Todo existing = todoRepositories.findById(id).orElseThrow();

        existing.setCompleted(!existing.isCompleted());

        todoRepositories.save(existing);

    }
}
