package com.example.nativecrud.repositories;

import com.example.nativecrud.entities.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepositories extends JpaRepository<Todo, Integer> {

}
