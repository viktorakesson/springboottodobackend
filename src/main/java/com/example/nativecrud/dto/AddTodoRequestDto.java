package com.example.nativecrud.dto;


import lombok.*;


public record AddTodoRequestDto(String title) {
}
