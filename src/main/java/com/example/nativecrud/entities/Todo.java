package com.example.nativecrud.entities;

import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotNull
    private String todo;

    @Column
    private boolean completed;

    public Todo (String todo) {
        this.todo = todo;
        this.completed = false;
    }

}
