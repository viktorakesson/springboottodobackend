package com.example.nativecrud.controllers;

import com.example.nativecrud.dto.AddTodoRequestDto;
import com.example.nativecrud.entities.Todo;
import com.example.nativecrud.services.TodoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService){
        this.todoService = todoService;
    }

    @GetMapping
    public List<Todo> findAll(){
        return todoService.findAll();
    }

    @PostMapping
    public Todo addNewTodo(@RequestBody AddTodoRequestDto addTodoRequestDto){
        return todoService.addNewTodo(addTodoRequestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteTodo(@PathVariable("id") String id){
        int intid = Integer.parseInt(id);
        todoService.deleteById(intid);
    }

    @GetMapping("/{id}")
    public Todo getTodoById(@PathVariable("id") String id){
        int intid = Integer.parseInt(id);
        return todoService.getTodoById(intid);
    }

    @PutMapping("/{id}")
    public void updateById(@PathVariable("id") String id) {
        int intid = Integer.parseInt(id);
        todoService.updateById(intid);

    }

}
